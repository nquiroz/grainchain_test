variable "region" {
  default = "us-west-2"
}

variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3Q/PZtHRln7dWJvcZgFj+JA//a5AZb9hAA2+Wc7enfIe4eBenAAiz6u9BJim1r355xUUwHkJ3TAWeOqsZr6bS/Kf745P2MmQ8FTQZLBV2ZTHqYtFdqmSrl2OBQR1XjoocIOkhST/qYYFDzB+5ROgBMR7t7Z5H2I6yFoZWIS+ymEOXUWf9BH7ocWE/xVlBio77jvANz96yh81EozPd+4fWj0GaL7ncWPD/5GGYgHl/bmMYdoevqGCqoNWY+0Fv4gdhyAutQKB/uY80K+VJGgBjvb0vXoOKxG2V7zw8IYVNI7TmaMCSl5YOldm0hrVWJ1aAtgrGn+xwlqHlFL9YA3JJLhWEE+hADeDQOfyVNmWxiu9ZvoND6ws/7Yh4oOy2/f9ccsmcUZMOJHsi+p61At0C9aaYZCDVDLJyB8dLkyTFPWbgtX/oslLQbHYM8JyAc9oJIkgdjvlMDQp4Sl/XCVNNtSV1uqbBiRdpD2b3WekDrzBd3hjdf8a3bK9Z/dRxr8U= jafir@MacBook-Air-Jafir.local"
}

variable "ami_image" {
  default = "ami-06f2f779464715dc5"
}

variable "instance_size" {
    default = "t3a.2xlarge"
}
