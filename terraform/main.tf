provider "aws" {
  profile = "default"
  region = var.region
}

#Generate KeyPair
resource "aws_key_pair" "tf_key" {
  key_name = "tf_key"
  public_key = var.public_key
}

#Generate ElasticIP
resource "aws_eip" "my_elastic_ip" {
  instance = "${aws_instance.MicroK8s_instance.id}"
  vpc = true
}

#Generate VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr_block
}
#Create a subnet
resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.my_vpc.id}"
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-west-2b"
}

#Create a subnet for Load balancer
resource "aws_subnet" "public2" {
  vpc_id = "${aws_vpc.my_vpc.id}"
  cidr_block = "10.0.10.0/24"
  availability_zone = "us-west-2a"
}
#Generate Internet Gateway
resource "aws_internet_gateway" "my_internet_gateway" {
  vpc_id = "${aws_vpc.my_vpc.id}"
}

#Generate route table
resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.my_vpc.id}"

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.my_internet_gateway.id}"
  }
}
#Associate the route table with the subnet
resource "aws_route_table_association" "public_route_ass" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}

#Generate a security group allowing port 22
resource "aws_security_group" "allow_ssh" {
  name = "allow_ssh"
  vpc_id = "${aws_vpc.my_vpc.id}"

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
}
#Generate a security group allowing port 80
resource "aws_security_group" "allow_http" {
  name = "allow_http"
  vpc_id = "${aws_vpc.my_vpc.id}"

  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = [
          "0.0.0.0/0"
      ]
  }
}
#Create an Load Balancer
resource "aws_lb" "my_load_balancer" {
  name = "my-LB"
  internal = false
  load_balancer_type = "application"
  subnets = [
      "${aws_subnet.public.id}",
      "${aws_subnet.public2.id}"
      ]
}
#Create ELB Target group
resource "aws_lb_target_group" "my-elb-target-group" {
  name = "my-elb-target-group"
  port = 80
  protocol = "HTTP"

  vpc_id = "${aws_vpc.my_vpc.id}"
}

#Create ELB Listener
resource "aws_lb_listener" "my_elb_frontend" {
  load_balancer_arn = "${aws_lb.my_load_balancer.arn}"
  port = "80"
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.my-elb-target-group.arn}"
  }
}
#Target the instance
resource "aws_lb_target_group_attachment" "my_attachment_group" {
  target_group_arn = "${aws_lb_target_group.my-elb-target-group.arn}"
  target_id = "${aws_instance.MicroK8s_instance.id}"
  port = 80
}

#Create EC2 Instance
resource "aws_instance" "MicroK8s_instance" {
  ami = var.ami_image
  instance_type = var.instance_size
  subnet_id = "${aws_subnet.public.id}"
  key_name = "${aws_key_pair.tf_key.key_name}"
  root_block_device {
      volume_size = "30"
  }
  vpc_security_group_ids = [
      "${aws_security_group.allow_ssh.id}",
      "${aws_security_group.allow_http.id}"
  ]
}

