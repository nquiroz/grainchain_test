#!/bin/bash
ssh ubuntu@44.226.253.88

if [ ! -d "grainchain_test" ]
then
    echo "Cloning repository from gitlab"
    git clone https://gitlab.com/nquiroz/grainchain_test.git
fi
cd grainchain_test 
echo "Update the repository with latest changes"
git checkout master && git pull origin master
echo "Deploy/Update helm chart"
export KUBECONFIG=/etc/microk8s/microk8s.conf
helm upgrade nginx mychart

